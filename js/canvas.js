/**
 * Created by BISO  -  www.biso.dk
 * Writer: bilde
 * Date: 30/10/11
 * Time: 00.58
 */

var imagePaths = ["../../images/faces/ali.jpeg", "../../images/faces/martina.jpeg", "../../images/faces/oliver.jpeg", "../../images/faces/patrick.jpeg", "../../images/faces/stef.jpeg", "../../images/faces/thomas.jpeg", "../../images/faces/william.jpeg"
, "../../images/faces/amrit.jpeg", "../../images/faces/afrin.jpeg", "../../images/faces/Ilija.jpeg", "../../images/faces/krølle.jpeg", "../../images/faces/lasse.jpeg"];
var showCanvas = null;
var theCanvas = null;
var strokePat = null;
var showCanvasCtx = null;
var img = document.createElement("img");
var currentImage = 0;
var revealTimer;

var start;


window.onload = function ()
{
    showCanvas = document.getElementById('showCanvas');
    showCanvasCtx = showCanvas.getContext('2d');
    img.setAttribute('width',showCanvas.width);
    img.setAttribute('height',showCanvas.height);
    switchImage();
    // Start af animation
    start = setInterval(switchImage,5000);
}

function switchImage()
{

    img.setAttribute('src',imagePaths[currentImage++]);
    if (currentImage >= imagePaths.length)
        currentImage = 0;
    showCanvasCtx.globalAlpha = 0.1;
    revealTimer = setInterval(revealImage,100);
}

function switchImageReverse()
{
    img.setAttribute('src',imagePaths[currentImage--]);
    if (currentImage <= 0)
        currentImage = imagePaths.length;
    showCanvasCtx.globalAlpha = 0.10;
    revealTimer = setInterval(revealImage,100);
}

function revealImage()
{
    //showCanvasCtx.save();
    showCanvasCtx.drawImage(img,0,0,showCanvas.width,showCanvas.height);
    showCanvasCtx.globalAlpha += 0.10;
    if (showCanvasCtx.globalAlpha >= 1.0)
        clearInterval(revealTimer);
    showCanvasCtx.restore();
    nerdLogo();
}

function pause()
{
    clearInterval(start);
}

function play()
{
    start = setInterval(switchImage,5000);
}

function back()
{
    switchImageReverse()
}

function forward()
{
    switchImage();
}

function nerdLogo()
{
    theCanvas = document.getElementById('kea');
    if (theCanvas && theCanvas.getContext)
    {
        var ctx = theCanvas.getContext("2d");
        if (ctx)
        {
            var theString = "NERD";
            ctx.font= "300pt Tahoma";
            strokePat = ctx.createPattern(showCanvas,"repeat");
            ctx.fillStyle = strokePat;
            ctx.fillText(theString, 30,300);

            ctx.shadowColor = "#bbbbbb";
            ctx.shadowBlur = 10;
            ctx.shadowOffsetX = 5;
            ctx.shadowOffsetY = 5;
        }
    }
}
